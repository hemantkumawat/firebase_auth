import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_test_example/screens/authentication/authentication.dart';
import 'package:firebase_test_example/screens/authentication/login.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import 'home_screen.dart';

class Wrapper extends StatelessWidget {
  const Wrapper({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    if (user != null) {
      return HomeScreen();
    } else {
      return Authentication();
    }
  }
}
